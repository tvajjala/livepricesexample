package com.nikolas.livepricesexample.business.ticker.control;

import com.nikolas.livepricesexample.business.ticker.entity.Ticker;
import javax.json.Json;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class TickerEncoder implements Encoder.Text<Ticker> {
    
    @Override
    public void init(EndpointConfig config) {
    }
    
    @Override
    public String encode(Ticker ticker) throws EncodeException {
        return Json.createObjectBuilder()
                .add("name", ticker.getName())
                .add("code", ticker.getCode())
                .add("price", ticker.getPrice())
                .add("timestamp", ticker.getDate().toString())
                .build().toString();
    }
    
    @Override
    public void destroy() {
    }
}
