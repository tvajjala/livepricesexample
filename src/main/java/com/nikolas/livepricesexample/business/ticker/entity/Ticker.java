package com.nikolas.livepricesexample.business.ticker.entity;

import java.time.LocalDateTime;

public class Ticker {
    
    private String name;
    private String code;
    private double price;
    private LocalDateTime date;

    public Ticker() {
    }

    public Ticker(String name, String code, double price, LocalDateTime date) {
        this.name = name;
        this.code = code;
        this.price = price;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
