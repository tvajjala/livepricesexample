package com.nikolas.livepricesexample.business.ticker.control;

import com.nikolas.livepricesexample.business.ticker.entity.Ticker;
import java.time.LocalDateTime;
import java.util.Random;
import javax.annotation.PostConstruct;

public class TickerGenerator {

    private Ticker ticker;
    private Random random;
    private final String STOCK_NAME = "AppleInc";
    private final String STOCK_CODE = "AAPL";
    private final double MIN_PRICE = 397.00;
    private final double MAX_PRICE = 400.00;

    @PostConstruct
    protected void init() {
        ticker = new Ticker();
        random = new Random();
    }

    public Ticker getTickerPrice() {
        ticker.setCode(STOCK_CODE);
        ticker.setName(STOCK_NAME);
        ticker.setPrice(getNextPrice());
        ticker.setDate(LocalDateTime.now());
        return ticker;
    }

    private double getNextPrice() {
        double randomValue = MIN_PRICE + (MAX_PRICE - MIN_PRICE) * random.nextDouble();
        return randomValue;
    }
}
