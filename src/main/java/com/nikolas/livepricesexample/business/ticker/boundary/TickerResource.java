package com.nikolas.livepricesexample.business.ticker.boundary;

import com.nikolas.livepricesexample.business.ticker.control.TickerEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/liveprices", encoders = TickerEncoder.class)
public class TickerResource {

    private static final Logger LOG = Logger.getLogger(TickerResource.class.getName());
    
    public static final List<Session> sessions = Collections.synchronizedList(new ArrayList<>());
    private final String CLOSE_MESSAGE = "close";

    @OnOpen
    public void onOpen(Session session) {
        if(!sessions.contains(session)) {
            LOG.log(Level.INFO, "New session was opened: {0}", session.getId());
            sessions.add(session);
        }
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        if (message.equalsIgnoreCase(CLOSE_MESSAGE)) {
            onClose(session);
        }
    }

    @OnClose
    public void onClose(Session session) {
        if(sessions.contains(session)) {
            LOG.log(Level.INFO, "Session was closed: {0}", session.getId());
            sessions.remove(session);
        }
    }
}
