package com.nikolas.livepricesexample.business.ticker.boundary;

import static com.nikolas.livepricesexample.business.ticker.boundary.TickerResource.sessions;
import com.nikolas.livepricesexample.business.ticker.control.TickerGenerator;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class TickerService {

    @Inject
    TickerGenerator tickerGenerator;
    
    @Schedule(second = "*/1", minute = "*", hour = "*", persistent = false)
    public void sendNewPrice() {
        sessions.parallelStream()
                    .filter(s -> s.isOpen())
                    .forEach(s -> {s.getAsyncRemote().sendObject(tickerGenerator.getTickerPrice());
        });
    }
}
