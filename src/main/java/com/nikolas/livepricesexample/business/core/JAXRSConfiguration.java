package com.nikolas.livepricesexample.business.core;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/resources")
public class JAXRSConfiguration extends Application {
    
}
