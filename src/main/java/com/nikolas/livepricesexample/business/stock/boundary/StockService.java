package com.nikolas.livepricesexample.business.stock.boundary;

import com.nikolas.livepricesexample.business.stock.control.StockDao;
import com.nikolas.livepricesexample.business.stock.entity.Stock;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class StockService {
    
    @Inject
    private StockDao stockDao;
    
    public List<Stock> getAllStocks() {
        return stockDao.findAll();
    }
}
