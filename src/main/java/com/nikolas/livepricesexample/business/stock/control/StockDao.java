package com.nikolas.livepricesexample.business.stock.control;

import com.nikolas.livepricesexample.business.core.BaseService;
import com.nikolas.livepricesexample.business.stock.entity.Stock;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class StockDao extends BaseService<Stock> {

    @PersistenceContext(unitName = "livepricesPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StockDao() {
        super(Stock.class);
    }
}
