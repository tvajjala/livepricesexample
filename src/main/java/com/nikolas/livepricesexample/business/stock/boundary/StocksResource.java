package com.nikolas.livepricesexample.business.stock.boundary;

import com.nikolas.livepricesexample.business.stock.entity.Stock;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/stocks")
public class StocksResource {
    
    @Inject
    private StockService stockService;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStocks() {
        GenericEntity<List<Stock>> stocks = new GenericEntity<List<Stock>>(stockService.getAllStocks()) {};
        return Response.ok(stocks).build(); 
    } 
}
