package com.nikolas.livepricesexample.ui.index;

import org.apache.wicket.Application;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.Url;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.UrlResourceReference;

public class IndexPage extends WebPage {

    public IndexPage(PageParameters parameters) {
        super(parameters);
    }
    
    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        response.render(JavaScriptHeaderItem.forReference(Application.get().getJavaScriptLibrarySettings().getJQueryReference()));
        response.render(JavaScriptHeaderItem.forReference(new UrlResourceReference(Url.parse("/LivePricesExample/static/js/bootstrap.min.js"))));
        response.render(JavaScriptHeaderItem.forReference(new UrlResourceReference(Url.parse("/LivePricesExample/static/js/highstock.js"))));
        response.render(JavaScriptHeaderItem.forReference(new UrlResourceReference(Url.parse("/LivePricesExample/static/js/charts.js")))); 
    }
}
