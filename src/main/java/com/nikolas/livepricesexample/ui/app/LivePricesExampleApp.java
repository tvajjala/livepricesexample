package com.nikolas.livepricesexample.ui.app;

import com.nikolas.livepricesexample.ui.index.IndexPage;
import org.apache.wicket.Page;
import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.cdi.CdiConfiguration;
import org.apache.wicket.protocol.http.WebApplication;

public class LivePricesExampleApp extends WebApplication {
    
    @Override
    public void init() {
        super.init();
        configureWicketCdi();
    }
    
    @Override
    public Class<? extends Page> getHomePage() {
        return IndexPage.class;
    }
    
    private void configureWicketCdi() {
        new CdiConfiguration().configure(this);
    }
    
    @Override
    public RuntimeConfigurationType getConfigurationType() {
        return RuntimeConfigurationType.DEVELOPMENT;
    }
}
