$(document).ready(function () {

    $.getJSON('http://localhost:8080/LivePricesExample/resources/stocks', function (data) {

        data = data.splice(1, data.length);
        var processedData = [];
        $(data).each(function (index, value) {
            processedData.push([
                Date.parse(value.date),
                parseFloat(value.open),
                parseFloat(value.high),
                parseFloat(value.low),
                parseFloat(value.close)
            ]);
        });
        processedData = processedData.reverse();

        $('#historicalChartContainer').highcharts('StockChart', {
            rangeSelector: {
                selected: 1
            },
            title: {
                text: 'AAPL Stock Price'
            },
            tooltip: {
                valueDecimals: 2
            },
            series: [{
                    type: 'candlestick',
                    name: 'AAPL Stock Price',
                    data: processedData,
                    step: true
                }]
        });
    });

    var connection;

    $('#startTicker').click(function () {
        var chart = $('#liveChartContainer').highcharts();
        connection = new WebSocket('ws://localhost:8080/LivePricesExample/liveprices');
        connection.onmessage = function (event) {
            var data = JSON.parse(event.data);
            var processedData = [];
            processedData.push([
                Date.parse(data.timestamp),
                parseFloat(data.price)
            ]);

            var series = chart.get(data.name);
            if (series) {
                series.addPoint({x: processedData[0][0], y: processedData[0][1]}, true, series.data.length > 25);
            }
            else {
                chart.addSeries({
                    name: [data.name],
                    id: data.name
                });
            }
        };
    });
    
    $('#stopTicker').click(function () {
        if(connection !== null) {
            connection.send("CLOSE");
        }
    });

    $('#liveChartContainer').highcharts('StockChart', {
        series: [],
        title: {
            text: 'Apple Stock Ticker'
        },
        rangeSelector: {
            buttons: [{
                    count: 1,
                    type: 'minute',
                    text: '1M'
                }, {
                    count: 5,
                    type: 'minute',
                    text: '5M'
                }, {
                    type: 'all',
                    text: 'All'
                }],
            inputEnabled: false,
            selected: 0
        },
        tooltip: {
            valueDecimals: 2
        }
    });

});